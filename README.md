<!--
  -- Historical ideas of future BSD and mascot
  --
  -- Copyright (c) 2015-2021 Márcio Silva <coadde@hyperbola.info>.
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions
  -- are met:
  -- 1. Redistributions of source code must retain the above copyright
  --    notice, this list of conditions and the following disclaimer.
  -- 2. Redistributions in binary form must reproduce the above copyright
  --    notice, this list of conditions and the following disclaimer in the
  --    documentation and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
  -- IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
  -- OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  -- IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT,
  -- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  -- NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  -- DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  -- THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  -- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
  -- THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  -->

#### This file only has historical ideas from 2015/09/20.

---

### Recursive Acronym Names:

All names are _recursive acronyms_, such as:

*   The initial letter __D__ mean __DAVIANA__.
*   __DAVIANA__ (__DAVIANA__'s **A** ser**VI**ce **A**nd **N**ot d**A**emon.):
<br/>
    Generic mascot that replace the _Beastie_ daemon mascot;<br/>
    It's a human girl, with cyan and white dress and<br/>
    and serving a water drink:
    *   On _DyfBOS-FreeIce_ uses __DAVIANA__ mascot,<br/>
        with _ice cubes_ on the drink.
    *   On _DyfBOS-LightNet_ logo, is the same with _opposing_ color,<br/>
        the red-orange to cyan-blue (__#f26711__ -> __#119cf2__) and uses<br/>
        __DAVIANA__ mascot with the _flag_ on the drink.
    *   On _DyfBOS-LibreAqua_, _Puffy_ will be renamed to _Puffree_,<br/>
        _without_ spines and with _opposing_ color,<br/>
        the yellow to blue (__#fff68e__ -> __#8f98ff__) or<br/>
        uses __DAVIANA__ mascot with the _Puffree_ inside in the drink.
    *   On _DyfBOS-AirBird_ mascot, will be use generic _bird_ with cyan color,
        <br/>or uses __DAVIANA__ mascot with the generic _bird_.
*   __DS4LiBSD__ (__DS4LiBSD__'s scripts for liberate __BSDs__):<br/>
    _Hipotetical_ scripts for _clean nonfree_ codes and files for any __BSD__.
*   __DyfBOS__ (__DyfBOS__'s
    full**Y** **F**ree **B**erkeley **O**perating **S**ystem):<br/>
    _Obsolete_ name for _fully free_ __BSD__ operating system.
*   __DyfBKS__ (__DyfBKS__'s
    full**Y** **F**ree **B**erkeley **K**ernel **S**oftware):<br/>
    _Obsolete_ name for _fully free_ __BSD__ kernel.
*   __DyfBOS-FreeIce__:
    _Fully free_ __FreeBSD__ operating system replacement name.
*   __DyfBKS-FreeIce__:
    _Fully free_ __FreeBSD__ kernel replacement name.
*   __DyfBOS-LightNet__:
    _Fully free_ __NetBSD__ operating system replacement name.
*   __DyfBKS-LightNet__:
    _Fully free_ __NetBSD__ kernel replacement name.
*   __DyfBOS-LibreAqua__:
    _Fully free_ __OpenBSD__ operating system replacement name.
*   __DyfBKS-LibreAqua__:
    _Fully free_ __OpenBSD__ kernel replacement name.
*   __DyfBOS-AirBird__:
    _Fully free_ __DragonFly BSD__ operating system replacement name.
*   __DyfBKS-AirBird__:
    _Fully free_ __DragonFly BSD__ kernel replacement name.
*   __&lt;distribution&gt;__ __GNU/k&#42;BSD&#42;__:
    _Fully free_ operating system distribution with<br/>
    __GNU__ userspace and a replacement __BSD__ kernel.

---

Licenses:
---------

*   Historical ideas of future BSD and mascot (2015-2021)<br/>
    by _[M&aacute;rcio Silva][COADDE]_ is under the terms:<br/>
    _[Simplified BSD license (BSD 2-clause license)][BSD2C]_.

[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

[BSD2C]: http://www.netbsd.org/about/redistribution.html#default
    "Simplified BSD License (BSD 2-clause license)"
